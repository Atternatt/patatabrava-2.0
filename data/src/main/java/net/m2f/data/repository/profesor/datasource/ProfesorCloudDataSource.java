/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.profesor.datasource;

import net.m2f.data.api.RestApi;
import net.m2f.data.database.DataBase;
import net.m2f.patatabrava.domain.model.Profesor;

import java.util.List;

import rx.Observable;

/**
 * A cloud implementation of {@link ProfesorDataSource}
 * @author Marc Moreno
 * @version 1.0
 */
public class ProfesorCloudDataSource implements ProfesorDataSource {

    RestApi api;
    DataBase dataBase;

    public ProfesorCloudDataSource(RestApi api, DataBase dataBase) {
        this.api = api;
        this.dataBase = dataBase;
    }

    @Override
    public Observable<List<Profesor>> getAllProfesors() {
        return api.getProfes(1)
                .compose(new FromProfesorListResponseToProfesorListWithDataBaseStoringPerFacultat(dataBase, 1));
    }

    @Override
    public Observable<List<Profesor>> getProfesFromFacultat(int idFacultat) {
        return api.getProfes(idFacultat)
                .compose(new FromProfesorListResponseToProfesorListWithDataBaseStoringPerFacultat(dataBase, idFacultat));
    }
}
