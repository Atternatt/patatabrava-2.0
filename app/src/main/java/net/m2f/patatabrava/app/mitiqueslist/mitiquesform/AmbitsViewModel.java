/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist.mitiquesform;

import android.databinding.ObservableField;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import net.m2f.patatabrava.domain.interactor.DefaultSubscriber;
import net.m2f.patatabrava.domain.interactor.UseCase;
import net.m2f.patatabrava.domain.model.Ambit;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class AmbitsViewModel {

    private final ObservableField<List<Ambit>> ambitList = new ObservableField<>();

    @Inject @Named("getAmbits")
    UseCase getAmbits;

    public void execute() {
        getAmbits.execute(new GetAmbitsViewModelInteractor());
    }


    @RxLogSubscriber private class GetAmbitsViewModelInteractor extends DefaultSubscriber<List<Ambit>> {
        @Override
        public void onCompleted() {
            super.onCompleted();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(List<Ambit> ambits) {
            super.onNext(ambits);
            ambitList.set(ambits);
        }
    }

}
