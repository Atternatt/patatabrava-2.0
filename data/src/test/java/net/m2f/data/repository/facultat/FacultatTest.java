/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.facultat;

import net.m2f.data.api.RestApi;
import net.m2f.data.api.RestApiImpl;
import net.m2f.data.api.entity.facultat.FacultatListResponse;
import net.m2f.data.database.DataBase;
import net.m2f.data.database.MockDatabase;
import net.m2f.data.repository.facultat.datasource.FacultatsDataSourceFactory;
import net.m2f.patatabrava.domain.model.Facultat;
import net.m2f.patatabrava.domain.repository.FacultatRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class FacultatTest {

    RestApi api;

    FacultatRepository repository;

    DataBase dataBase;

    @Before
    public void setUp() throws Exception {
        api = new RestApiImpl(new OkHttpClient());

        dataBase = new MockDatabase();

        repository = new FacultatRepositoryImpl(new FacultatsDataSourceFactory(api, dataBase));
    }

    @Test
    public void testGetFacultatsFromUniversitat() throws Exception {
        Observable<List<Facultat>> facultatsObservable = repository.getFacultatsFromUniversitat(1);

        TestSubscriber<List<Facultat>> testSubscriber = new TestSubscriber<>();


        facultatsObservable.subscribe(testSubscriber);


        testSubscriber.awaitTerminalEvent();

        String observingThread = testSubscriber.getLastSeenThread().getName();

        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();

        List<List<Facultat>> onNextEvents = testSubscriber.getOnNextEvents();
        List<Throwable> onErrorEvents = testSubscriber.getOnErrorEvents();

        assertTrue(onErrorEvents.isEmpty());
        assertTrue(!onNextEvents.isEmpty());
        assertTrue(onNextEvents.size() == 1);
        //once we get the values a complete event is sent
        testSubscriber.assertCompleted();
    }

    @Test
    public void testApiGetFacultats() {
        Observable<Result<FacultatListResponse>> facultats = api.getFacultats(1);

        TestSubscriber<Result<FacultatListResponse>> testSubscriber = new TestSubscriber<>();

        facultats.subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertNoErrors();

        List<Result<FacultatListResponse>> onNextEvents = testSubscriber.getOnNextEvents();
        List<Throwable> onErrorEvents = testSubscriber.getOnErrorEvents();

        assertTrue(onErrorEvents.isEmpty());
        assertTrue(!onNextEvents.isEmpty());
        assertTrue(onNextEvents.size() == 1);
        //once we get the values a complete event is sent
        testSubscriber.assertCompleted();
    }
}