/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.database;

import net.m2f.data.database.model.AmbitDBO;
import net.m2f.data.database.model.FacultatDBO;
import net.m2f.data.database.model.ProfesorDBO;
import net.m2f.data.database.model.UniversitatDBO;
import net.m2f.patatabrava.domain.model.Ambit;
import net.m2f.patatabrava.domain.model.Facultat;
import net.m2f.patatabrava.domain.model.Profesor;
import net.m2f.patatabrava.domain.model.Universitat;

import java.util.List;

import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class MockDatabase implements DataBase {

    @Override
    public Observable<List<ProfesorDBO>> getProfes() {
        return Observable.empty();
    }

    @Override
    public Observable<List<ProfesorDBO>> getProfesFromFacultat(int idFacultat) {
        return Observable.empty();
    }

    @Override
    public void storeProfe(Profesor profesor, int facultat) {

    }

    @Override
    public void storeProfes(List<Profesor> profesors, int facultat) {

    }

    @Override
    public Observable<List<FacultatDBO>> getFacultatsFromUniversitat(int idUniversitat) {
        return Observable.empty();
    }

    @Override
    public void storeFacultat(Facultat facultat, int universitat) {

    }

    @Override
    public void storeFacultats(List<Facultat> facultats, int universitat) {

    }

    @Override
    public Observable<List<UniversitatDBO>> getUniveristatsFromAmbit(int ambitId) {
        return Observable.empty();
    }

    @Override
    public void storeUniversitatFromAmbit(Universitat universitat, int ambit) {

    }

    @Override
    public void storeUniversitatsFromAmbit(List<Universitat> universitats, int ambit) {

    }

    @Override
    public Observable<List<AmbitDBO>> getAmbits() {
        return Observable.empty();
    }

    @Override
    public void storeAmbit(Ambit ambit) {

    }

    @Override
    public void storeAmbits(List<Ambit> ambits) {

    }
}
