/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist.votarmitica;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.databinding.BindingAdapter;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.view.ViewAnimationUtils;
import android.widget.Toast;

import net.m2f.patatabrava.R;
import net.m2f.patatabrava.domain.model.Mitica;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public final class VotarMiticaViewModelBinding {

    @BindingAdapter("mitica")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void votar(CardView cardView, Mitica mitica) {

        // TODO: 27/6/16 comprovar si la mitica es de la facultat de l'usuari.
        if (mitica.isVoted()) {
            // FIXME: 27/6/16 si el usuari no està logat llavors sempre surten com si no estiguessin votades, encara que posi que ho estan.
            cardView.setCardBackgroundColor(cardView.getContext().getResources().getColor(R.color.voted_background));
        } else {
            cardView.setCardBackgroundColor(cardView.getContext().getResources().getColor(R.color.primary_light));
        }

        if (mitica.isVoted()) {

            cardView.setOnClickListener(v -> {
                int finalRadius = Math.max(cardView.getWidth(), cardView.getHeight());

                cardView.setCardBackgroundColor(v.getContext().getResources().getColor(R.color.color_accent));
                final Animator anim = ViewAnimationUtils.createCircularReveal(cardView, cardView.getWidth()/2, cardView.getHeight()/2, 0, finalRadius);
                anim.setDuration(500);
                Animator.AnimatorListener animatorListener = new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        Toast.makeText(cardView.getContext(),"Votada",Toast.LENGTH_SHORT).show();
                        mitica.setVoted(true);
                    }
                };
                anim.addListener(animatorListener);
                anim.start();
            });

        }
    }
}
