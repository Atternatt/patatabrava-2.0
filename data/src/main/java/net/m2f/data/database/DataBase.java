/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.database;

import net.m2f.data.database.model.AmbitDBO;
import net.m2f.data.database.model.FacultatDBO;
import net.m2f.data.database.model.ProfesorDBO;
import net.m2f.data.database.model.UniversitatDBO;
import net.m2f.patatabrava.domain.model.Ambit;
import net.m2f.patatabrava.domain.model.Facultat;
import net.m2f.patatabrava.domain.model.Profesor;
import net.m2f.patatabrava.domain.model.Universitat;

import java.util.List;

import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public interface DataBase {

    //region Profesors
    Observable<List<ProfesorDBO>> getProfes();

    Observable<List<ProfesorDBO>> getProfesFromFacultat(int idFacultat);

    void storeProfe(Profesor profesor, int facultatId);

    void storeProfes(List<Profesor> profesors, int facultatId);
    //endregion

    //region Facultats
    Observable<List<FacultatDBO>> getFacultatsFromUniversitat(int idUniversitat);

    void storeFacultat(Facultat facultat, int universitatId);

    void storeFacultats(List<Facultat> facultats, int universitatId);
    //endregion

    //region Universitats
    Observable<List<UniversitatDBO>> getUniveristatsFromAmbit(int ambitId);

    void storeUniversitatFromAmbit(Universitat universitat, int ambitId);

    void storeUniversitatsFromAmbit(List<Universitat> universitats, int ambitId);
    //endregion

    //region Ambits
    Observable<List<AmbitDBO>> getAmbits();

    void storeAmbit(Ambit ambit);

    void storeAmbits(List<Ambit> ambits);
    //endregion





}
