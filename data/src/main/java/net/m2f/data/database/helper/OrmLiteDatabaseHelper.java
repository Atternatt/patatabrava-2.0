/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import net.m2f.data.database.model.AmbitDBO;
import net.m2f.data.database.model.FacultatDBO;
import net.m2f.data.database.model.ProfesorDBO;
import net.m2f.data.database.model.UniversitatDBO;

import java.sql.SQLException;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class OrmLiteDatabaseHelper extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "patatabrava.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the SimpleData table
    private Dao<ProfesorDBO, Integer> simpleProfesorDao = null;
    private Dao<FacultatDBO, Integer> simpleFacultatDao = null;
    private Dao<UniversitatDBO, Integer> simpleUniversitatDao = null;
    private Dao<AmbitDBO, Integer> simpleAmbitDao = null;

    public OrmLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(OrmLiteDatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, ProfesorDBO.class);
            TableUtils.createTable(connectionSource, FacultatDBO.class);
            TableUtils.createTable(connectionSource, UniversitatDBO.class);
            TableUtils.createTable(connectionSource, AmbitDBO.class);
        } catch (SQLException e) {
            Log.e(OrmLiteDatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }

    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(OrmLiteDatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, ProfesorDBO.class, true);
            TableUtils.dropTable(connectionSource, FacultatDBO.class, true);
            TableUtils.dropTable(connectionSource, UniversitatDBO.class, true);
            TableUtils.dropTable(connectionSource, AmbitDBO.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(OrmLiteDatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our {@link ProfesorDBO} class. It will create it or just give the cached
     * value.
     */
    public Dao<ProfesorDBO, Integer> getProfesorDao() throws SQLException {
        if (simpleProfesorDao == null) {
            simpleProfesorDao = getDao(ProfesorDBO.class);
        }
        return simpleProfesorDao;
    }

    public Dao<FacultatDBO, Integer> getFacultatDao() throws SQLException {
        if (simpleFacultatDao == null) {
            simpleFacultatDao = getDao(FacultatDBO.class);
        }
        return simpleFacultatDao;
    }

    public Dao<UniversitatDBO, Integer> getUniversitatDao() throws SQLException {
        if (simpleUniversitatDao == null) {
            simpleUniversitatDao = getDao(UniversitatDBO.class);
        }
        return simpleUniversitatDao;
    }

    public Dao<AmbitDBO, Integer> getAmbitDao() throws SQLException {
        if (simpleAmbitDao == null) {
            simpleAmbitDao = getDao(AmbitDBO.class);
        }
        return simpleAmbitDao;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        simpleProfesorDao = null;
        simpleAmbitDao = null;
        simpleFacultatDao = null;
        simpleUniversitatDao = null;
    }
}
