/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.di.modules;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import net.m2f.data.api.RestApi;
import net.m2f.data.api.RestApiImpl;
import net.m2f.data.database.DataBase;
import net.m2f.data.database.OrmLiteDataBase;
import net.m2f.data.repository.mitica.MiticaRepositoryImpl;
import net.m2f.patatabrava.AndroidApplication;
import net.m2f.patatabrava.domain.executor.PostExecutionThread;
import net.m2f.patatabrava.domain.repository.MiticaRepository;
import net.m2f.patatabrava.domain.retrypolicy.RetryPolicy;
import net.m2f.patatabrava.executor.UIThread;
import net.m2f.patatabrava.retrypolicy.RetryWithConnectivityIncremental;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@Module
public class AppModule {

    private final AndroidApplication application;

    public AppModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public OkHttpClient provideClient() {
        return new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return this.application.getApplicationContext();
    }

    @Provides
    @Singleton
    public RestApi provideRestApi(RestApiImpl restApi) {
        return restApi;
    }

    @Provides
    @Singleton
    public DataBase provideDatabase(OrmLiteDataBase dataBase) {
        return dataBase;
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    public MiticaRepository providesMiticaRepository(MiticaRepositoryImpl miticaRepository) {
        return miticaRepository;
    }

    @Provides
    @Singleton
    public RetryPolicy providesRetryPolicy() {
        return new RetryWithConnectivityIncremental(3, 7, TimeUnit.SECONDS);
    }
}
