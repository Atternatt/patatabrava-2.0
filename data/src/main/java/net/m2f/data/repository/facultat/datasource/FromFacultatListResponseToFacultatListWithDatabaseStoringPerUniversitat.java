/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.facultat.datasource;

import net.m2f.data.api.entity.facultat.FacultatListResponse;
import net.m2f.data.api.entity.mapper.EntityFacultatToBO;
import net.m2f.data.database.DataBase;
import net.m2f.patatabrava.domain.model.Facultat;

import java.util.List;

import retrofit2.adapter.rxjava.Result;
import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class FromFacultatListResponseToFacultatListWithDatabaseStoringPerUniversitat implements Observable.Transformer<Result<FacultatListResponse>, List<Facultat>> {

    DataBase database;
    int idUniversitat;

    public FromFacultatListResponseToFacultatListWithDatabaseStoringPerUniversitat(DataBase database, int univesitatId) {
        this.database = database;
        this.idUniversitat = univesitatId;
    }

    @Override
    public Observable<List<Facultat>> call(Observable<Result<FacultatListResponse>> facultatListResponseObservable) {
        return facultatListResponseObservable.map(facultatListResponseResult -> facultatListResponseResult.response().body().getEntityFacultats())
                .flatMap(Observable::from)
                .map(EntityFacultatToBO::transform)
                .toList()
                .doOnNext(facultats -> database.storeFacultats(facultats, idUniversitat));
    }
}
