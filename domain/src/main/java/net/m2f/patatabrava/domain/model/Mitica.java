/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.domain.model;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class Mitica {

    private String stringProfe;

    private String stringUniShort;

    private String idMitica;

    private String stringMitica;

    private String stringFac;

    private int points;

    private int voted;

    public Mitica(String stringProfe, String stringUniShort, String idMitica, String stringMitica, String stringFac, int points, int voted) {
        this.stringProfe = stringProfe;
        this.stringUniShort = stringUniShort;
        this.idMitica = idMitica;
        this.stringMitica = stringMitica;
        this.stringFac = stringFac;
        this.points = points;
        this.voted = voted;
    }

    public String getStringProfe() {
        return stringProfe;
    }

    public String getStringUniShort() {
        return stringUniShort;
    }

    public String getIdMitica() {
        return idMitica;
    }

    public String getStringMitica() {
        return stringMitica;
    }

    public String getStringFac() {
        return stringFac;
    }

    public int getPoints() {
        return points;
    }

    public boolean isVoted() {
        return voted == 1;
    }

    public void setVoted(boolean voted) {
        this.voted = voted ? 1 : 0;
    }

    @Override
    public String toString() {
        return "Mitica{" +
                "stringProfe='" + stringProfe + '\'' +
                ", stringUniShort='" + stringUniShort + '\'' +
                ", idMitica='" + idMitica + '\'' +
                ", stringMitica='" + stringMitica + '\'' +
                ", stringFac='" + stringFac + '\'' +
                ", points='" + points + '\'' +
                ", voted='" + isVoted() + '\'' +
                '}';
    }
}
