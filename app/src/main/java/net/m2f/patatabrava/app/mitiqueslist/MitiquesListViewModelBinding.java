/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import net.m2f.patatabrava.app.mitiqueslist.adapter.MiticaAdapter;
import net.m2f.patatabrava.app.mitiqueslist.rx.RxRecyclerView;
import net.m2f.patatabrava.domain.model.Mitica;

import java.util.List;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public final class MitiquesListViewModelBinding {

    @BindingAdapter("mitiquesList")
    public static void loadValuesToRecyclerView(RecyclerView recyclerView, List<Mitica> oldList, List<Mitica> newList) {
        if (null == oldList) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            MiticaAdapter adapter = new MiticaAdapter(newList);
            recyclerView.setAdapter(adapter);
        } else {
            ((MiticaAdapter) recyclerView.getAdapter()).addMitiques(newList);
        }
    }

    @BindingAdapter("bind")
    public static void bindRecyclerViewToViewModel(RecyclerView recyclerView, MitiquesListViewModel oldBindable, MitiquesListViewModel newBindable) {
        if (null != oldBindable) {
            oldBindable.unBind();
        }
        newBindable.bind(RxRecyclerView.infiniteScrollEvents(recyclerView));
    }

    @BindingAdapter("title")
    public static void loadTitle(Toolbar toolbar, String title) {
        toolbar.setTitle(title);
    }
}
