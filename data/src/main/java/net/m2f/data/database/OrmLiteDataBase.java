/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.database;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.SelectArg;

import net.m2f.data.database.helper.OrmLiteDatabaseHelper;
import net.m2f.data.database.model.AmbitDBO;
import net.m2f.data.database.model.FacultatDBO;
import net.m2f.data.database.model.ProfesorDBO;
import net.m2f.data.database.model.UniversitatDBO;
import net.m2f.patatabrava.domain.model.Ambit;
import net.m2f.patatabrava.domain.model.Facultat;
import net.m2f.patatabrava.domain.model.Profesor;
import net.m2f.patatabrava.domain.model.Universitat;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@Singleton
public class OrmLiteDataBase implements DataBase {

    OrmLiteDatabaseHelper helper;
    private Context context;

    @Inject
    public OrmLiteDataBase(Context context) {
        this.context = context;
    }

    public OrmLiteDatabaseHelper getHelper() {
        if (helper == null) {
            helper = new OrmLiteDatabaseHelper(context);
        }
        return helper;
    }

    //region Profesors
    @Override
    public Observable<List<ProfesorDBO>> getProfes() {

        Observable<List<ProfesorDBO>> observable = Observable.create(subscriber -> {
            try {
                List<ProfesorDBO> profes = getHelper().getProfesorDao().queryForAll();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(profes);
                    subscriber.onCompleted();
                }
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });

        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    public Observable<List<ProfesorDBO>> getProfesFromFacultat(int idFacultat) {

        Observable<List<ProfesorDBO>> observable = Observable.create(subscriber -> {
            try {
                Dao<ProfesorDBO, Integer> profesorDao = getHelper().getProfesorDao();
                SelectArg selectArgument = new SelectArg();
                PreparedQuery<ProfesorDBO> preparedQuery = profesorDao.queryBuilder().where().eq(ProfesorDBO.FIELD_FACULTAT_ID, selectArgument).prepare();

                selectArgument.setValue(idFacultat);

                List<ProfesorDBO> profes = profesorDao.query(preparedQuery);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(profes);
                    subscriber.onCompleted();
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        observable.subscribeOn(Schedulers.io());

        return observable;
    }

    @Override
    public void storeProfe(Profesor profesor, int facultatId) {
        try {
            Dao<ProfesorDBO, Integer> dao = getHelper().getProfesorDao();
            ProfesorDBO profesorDBO = new ProfesorDBO(profesor.getNom(), profesor.getId(), facultatId);
            dao.createIfNotExists(profesorDBO);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void storeProfes(List<Profesor> profesors, int facultatId) {

        for (Profesor profesor : profesors) {
            storeProfe(profesor, facultatId);
        }

    }
    //endregion


    //region Facultats
    @Override
    public Observable<List<FacultatDBO>> getFacultatsFromUniversitat(int idUniversitat) {
        Observable<List<FacultatDBO>> observable = Observable.create(subscriber -> {
            try {
                Dao<FacultatDBO, Integer> profesorDao = getHelper().getFacultatDao();
                SelectArg selectArgument = new SelectArg();
                PreparedQuery<FacultatDBO> preparedQuery = profesorDao.queryBuilder().where().eq(FacultatDBO.FIELD_UNIVERSITAT_ID, selectArgument).prepare();

                selectArgument.setValue(idUniversitat);

                List<FacultatDBO> facultats = profesorDao.query(preparedQuery);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(facultats);
                    subscriber.onCompleted();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        observable.subscribeOn(Schedulers.io());

        return observable;
    }

    @Override
    public void storeFacultat(Facultat facultat, int universitatId) {
        try {
            Dao<FacultatDBO, Integer> facultatDao = getHelper().getFacultatDao();
            FacultatDBO facultatDBO = new FacultatDBO(facultat.getId(), facultat.getNom(), universitatId);
            facultatDao.createIfNotExists(facultatDBO);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void storeFacultats(List<Facultat> facultats, int universitatId) {
        for (Facultat facultat : facultats) {
            storeFacultat(facultat, universitatId);
        }
    }
    //endregion


    //region Universitats
    @Override
    public Observable<List<UniversitatDBO>> getUniveristatsFromAmbit(int ambitId) {
        Observable<List<UniversitatDBO>> observable = Observable.create(subscriber -> {
            try {
                Dao<UniversitatDBO, Integer> profesorDao = getHelper().getUniversitatDao();
                SelectArg selectArgument = new SelectArg();
                PreparedQuery<UniversitatDBO> preparedQuery = profesorDao.queryBuilder().where().eq(FacultatDBO.FIELD_UNIVERSITAT_ID, selectArgument).prepare();

                selectArgument.setValue(ambitId);

                List<UniversitatDBO> universitats = profesorDao.query(preparedQuery);

                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(universitats);
                    subscriber.onCompleted();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        observable.subscribeOn(Schedulers.io());

        return observable;
    }

    @Override
    public void storeUniversitatFromAmbit(Universitat universitat, int ambitId) {

        try {
            Dao<UniversitatDBO, Integer> dao = getHelper().getUniversitatDao();
            UniversitatDBO universitatDBO = new UniversitatDBO(universitat.getId(), universitat.getNom(), ambitId);
            dao.createIfNotExists(universitatDBO);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void storeUniversitatsFromAmbit(List<Universitat> universitats, int ambitId) {
        for (Universitat universitat : universitats) {
            storeUniversitatFromAmbit(universitat, ambitId);
        }
    }
    //endregion


    //region Ambits
    @Override
    public Observable<List<AmbitDBO>> getAmbits() {
        Observable<List<AmbitDBO>> observable = Observable.create(subscriber -> {
            try {
                List<AmbitDBO> ambits = getHelper().getAmbitDao().queryForAll();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(ambits);
                    subscriber.onCompleted();
                }
            } catch (SQLException e) {
                subscriber.onError(e);
            }
        });

        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    public void storeAmbit(Ambit ambit) {
        try {
            Dao<AmbitDBO, Integer> dao = getHelper().getAmbitDao();
            AmbitDBO ambitDBO = new AmbitDBO(ambit.getId(), ambit.getNom());
            dao.createIfNotExists(ambitDBO);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void storeAmbits(List<Ambit> ambits) {
        for (Ambit ambit : ambits) {
            storeAmbit(ambit);
        }
    }
    //endregion
}
