/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.univeristat;

import net.m2f.data.repository.univeristat.datasource.UniversitatDatasourceFactory;
import net.m2f.patatabrava.domain.model.Universitat;
import net.m2f.patatabrava.domain.repository.UniversitatRepository;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class UniversitatRepositoryImpl implements UniversitatRepository {

    UniversitatDatasourceFactory factory;

    public UniversitatRepositoryImpl(UniversitatDatasourceFactory factory) {
        this.factory = factory;
    }

    @Override
    public Observable<List<Universitat>> getUniversitatsFromAmbit(int ambitId) {
        return Observable.concat(factory.generateLocalDataSource().getUniversitatsFromAmbit(ambitId),
                factory.generateCloudDataSource().getUniversitatsFromAmbit(ambitId))
                .takeFirst(universitats -> !universitats.isEmpty())
                .subscribeOn(Schedulers.io());
    }
}
