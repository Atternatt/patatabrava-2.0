/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.ambit;

import net.m2f.data.api.RestApi;
import net.m2f.data.api.RestApiImpl;
import net.m2f.data.database.DataBase;
import net.m2f.data.database.MockDatabase;
import net.m2f.data.repository.ambit.datasource.AmbitDataSourceFactory;
import net.m2f.patatabrava.domain.model.Ambit;
import net.m2f.patatabrava.domain.repository.AmbitRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import okhttp3.OkHttpClient;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class AmbitTest {

    RestApi api;

    DataBase database;

    AmbitRepository repository;

    @Before
    public void setUp() throws Exception {

        api = new RestApiImpl(new OkHttpClient());

        database = new MockDatabase();

        repository = new AmbitRepositoryImpl(new AmbitDataSourceFactory(api, database));
    }

    @Test
    public void testGetAmbits() throws Exception {

        Observable<List<Ambit>> listObservable = repository.getAmbits();

        TestSubscriber<List<Ambit>> testSubscriber = new TestSubscriber<>();

        listObservable.subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();

        String observingThread = testSubscriber.getLastSeenThread().getName();

        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();

        List<List<Ambit>> onNextEvents = testSubscriber.getOnNextEvents();

        assertTrue(!onNextEvents.isEmpty());

        testSubscriber.assertCompleted();


    }
}