/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist;

import android.databinding.ObservableField;

import net.m2f.patatabrava.app.Bindable;
import net.m2f.patatabrava.di.PerActivity;
import net.m2f.patatabrava.domain.interactor.DefaultSubscriber;
import net.m2f.patatabrava.domain.interactor.UseCaseSubject;
import net.m2f.patatabrava.domain.model.Mitica;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@PerActivity
public class MitiquesListViewModel implements Bindable<Integer> {

    public final ObservableField<List<Mitica>> mitiquesList = new ObservableField<>();
    public ObservableField<String> titol = new ObservableField<>();
    UseCaseSubject getMitiques;

    @Inject
    public MitiquesListViewModel(UseCaseSubject getMitiques) {
        this.getMitiques = getMitiques;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bind(Observable<Integer> eventObservable) {
        getMitiques.unsubscribe();
        getMitiques.bind(eventObservable, new GetMitiquesListInteractor());
    }

    public void setTitol(String titol) {
        this.titol.set(titol);
    }

    @Override
    public void unBind() {
        getMitiques.unsubscribe();
    }

    private class GetMitiquesListInteractor extends DefaultSubscriber<List<Mitica>> {

        @Override
        public void onCompleted() {
            super.onCompleted();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(List<Mitica> miticas) {
            mitiquesList.set(miticas);
        }
    }
}
