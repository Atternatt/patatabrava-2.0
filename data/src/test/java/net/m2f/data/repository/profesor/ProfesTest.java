/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.profesor;

import net.m2f.data.api.RestApi;
import net.m2f.data.api.RestApiImpl;
import net.m2f.data.api.entity.profesor.ProfesorListResponse;
import net.m2f.data.database.DataBase;
import net.m2f.data.database.MockDatabase;
import net.m2f.data.repository.profesor.datasource.ProfesorDataSourceFactory;
import net.m2f.patatabrava.domain.model.Profesor;
import net.m2f.patatabrava.domain.repository.ProfesRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.containsString;

public class ProfesTest {

    RestApi api;

    ProfesRepository repository;

    DataBase dataBase;

    @Before
    public void setUp() {
        api = new RestApiImpl(new OkHttpClient());
        dataBase = new MockDatabase();
        repository = new ProfesRepositoryImpl(new ProfesorDataSourceFactory(api, dataBase));
    }

    @Test
    public void testGetProfesWithRealConnection() {
        Observable<List<Profesor>> profesObservable = repository.getProfes(1);

        TestSubscriber<List<Profesor>> testSubscriber = new TestSubscriber<>();

        profesObservable.subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();

        String observingThread = testSubscriber.getLastSeenThread().getName();

        assertThat(observingThread, containsString("RxCachedThreadScheduler"));
        testSubscriber.assertNoErrors();

        List<List<Profesor>> onNextEvents = testSubscriber.getOnNextEvents();
        assertTrue(!onNextEvents.isEmpty());
        assertTrue(onNextEvents.size() == 1);

        testSubscriber.assertCompleted();
    }

    @Test
    public void testApiGetrpofes() {
        Observable<Result<ProfesorListResponse>> resultObservable = api.getProfes(1);

        TestSubscriber<Result<ProfesorListResponse>> subscriber = new TestSubscriber<>();

        resultObservable.subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        subscriber.assertNoErrors();

        List<Result<ProfesorListResponse>> onNextEvents = subscriber.getOnNextEvents();

        assertTrue(!onNextEvents.isEmpty());
        assertTrue(onNextEvents.size() == 1);

        subscriber.assertCompleted();
    }
}