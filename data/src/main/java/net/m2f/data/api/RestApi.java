/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.api;

import net.m2f.data.api.entity.ambit.AmbitListResponse;
import net.m2f.data.api.entity.facultat.FacultatListResponse;
import net.m2f.data.api.entity.mitica.MitiquesListResponse;
import net.m2f.data.api.entity.profesor.ProfesorListResponse;
import net.m2f.data.api.entity.universitat.UniversitatListResponse;

import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mmoreno on 25/5/16.
 */

public interface RestApi {

    String TOKEN = "f5b1b8821979954710d9bafbfc5cbf54";
    String BASE_URL = "http://www.patatabrava.com/api/";

    @GET("get_profes.php")
    Observable<Result<ProfesorListResponse>> getProfes(@Query("id_facultat") int idFacultat);

    @GET("get_facultats.php")
    Observable<Result<FacultatListResponse>> getFacultats(@Query("id_universitat") int idUniversitat);

    @GET("get_unis.php")
    Observable<Result<UniversitatListResponse>> getUniversitats(@Query("id_ambit") int idAmbit);

    @GET("get_ambits.php")
    Observable<Result<AmbitListResponse>> getAmbits();

    @GET("get_mitiques.php")
    Observable<Result<MitiquesListResponse>> getMitiques(@Query("page") int page, @Query("id_fac") int idFac, @Query("id_profe") int idProfe, @Query("top") int top);

}
