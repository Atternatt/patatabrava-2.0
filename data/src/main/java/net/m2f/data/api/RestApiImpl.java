/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.api;

import net.m2f.data.api.entity.ambit.AmbitListResponse;
import net.m2f.data.api.entity.facultat.FacultatListResponse;
import net.m2f.data.api.entity.mitica.MitiquesListResponse;
import net.m2f.data.api.entity.profesor.ProfesorListResponse;
import net.m2f.data.api.entity.universitat.UniversitatListResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.Result;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mmoreno on 25/5/16.
 */

@Singleton
public final class RestApiImpl implements RestApi {

    /**The rest service.*/
    RestApi service;

    @Inject
    public RestApiImpl(OkHttpClient client) {
        configureApi(client);
    }

    /**
     * Configure the service.
     */
    private void configureApi(OkHttpClient client) {
        service = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build()
                .create(RestApi.class);
    }

    @Override
    public Observable<Result<ProfesorListResponse>> getProfes(int idFacultat) {
        return service.getProfes(idFacultat);
    }

    @Override
    public Observable<Result<FacultatListResponse>> getFacultats(int idUniversitat) {
        return service.getFacultats(idUniversitat);
    }

    @Override
    public Observable<Result<UniversitatListResponse>> getUniversitats(int idAmbit) {
        return service.getUniversitats(idAmbit);
    }

    @Override
    public Observable<Result<AmbitListResponse>> getAmbits() {
        return service.getAmbits();
    }

    @Override
    public Observable<Result<MitiquesListResponse>> getMitiques(@Query("page") int page, @Query("id_fac") int idFac, @Query("id_profe") int idProfe, @Query("top") int top) {
        return service.getMitiques(page, idFac, idProfe, top);
    }
}
