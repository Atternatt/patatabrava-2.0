/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.m2f.patatabrava.R;
import net.m2f.patatabrava.databinding.RowMiticaBinding;
import net.m2f.patatabrava.domain.model.Mitica;

import java.util.List;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class MiticaAdapter extends RecyclerView.Adapter<MiticaAdapter.MiticaViewHolder>{

    List<Mitica> mitiques;

    public MiticaAdapter(List<Mitica> mitiques) {
        this.mitiques = mitiques;
    }

    public void setMitiques(List<Mitica> mitiques) {
        this.mitiques = mitiques;
        notifyDataSetChanged();
    }

    public void addMitiques(List<Mitica> mitiques) {
        int mitiquesSize = this.mitiques.size();
        this.mitiques.addAll(mitiques);
        notifyItemRangeInserted(mitiquesSize, mitiques.size());
    }

    @Override
    public MiticaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RowMiticaBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_mitica
                , parent, false);

        return new MiticaViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MiticaViewHolder holder, int position) {
        holder.bind(mitiques.get(position));
    }

    @Override
    public int getItemCount() {
        return mitiques == null ? 0 : mitiques.size();
    }

    class MiticaViewHolder extends RecyclerView.ViewHolder {

        private RowMiticaBinding binding;

        public MiticaViewHolder(RowMiticaBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Mitica mitica) {
            binding.setMitica(mitica);
        }
    }
}
