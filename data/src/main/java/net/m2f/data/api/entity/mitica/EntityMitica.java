/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.api.entity.mitica;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item")
public class EntityMitica {

    @Element(name = "id_mitica")
    private String idMitica;

    @Element(name = "string_mitica")
    private String stringMitica;

    @Element(name = "string_profe")
    private String stringProfe;

    @Element(name = "string_fac")
    private String stringFac;

    @Element(name = "string_uni_short")
    private String stringUniShort;

    @Element(name = "points", required = false )
    private int points;

    @Element(name = "voted")
    private int voted;

    public String getStringProfe() {
        return stringProfe;
    }

    public String getStringUniShort() {
        return stringUniShort;
    }

    public String getIdMitica() {
        return idMitica;
    }

    public String getStringMitica() {
        return stringMitica;
    }

    public String getStringFac() {
        return stringFac;
    }

    public int getPoints() {
        return points;
    }

    public int getVoted() {
        return voted;
    }

    @Override
    public String toString() {
        return "Mitica{" +
                "stringProfe='" + stringProfe + '\'' +
                ", stringUniShort='" + stringUniShort + '\'' +
                ", idMitica='" + idMitica + '\'' +
                ", stringMitica='" + stringMitica + '\'' +
                ", stringFac='" + stringFac + '\'' +
                ", points='" + points + '\'' +
                ", voted='" + voted + '\'' +
                '}';
    }
}