/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.repository.mitica;

import net.m2f.data.api.RestApi;
import net.m2f.data.api.RestApiImpl;
import net.m2f.data.repository.mitica.datasource.MitiquesDataSourceFactory;
import net.m2f.patatabrava.domain.model.Mitica;
import net.m2f.patatabrava.domain.repository.MiticaRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class MiticaRepositoryImplTest {

    RestApi api;

    MiticaRepository repository;

    @Before
    public void setUp() throws Exception {

        api = new RestApiImpl(new OkHttpClient());

        repository = new MiticaRepositoryImpl(new MitiquesDataSourceFactory(api));
    }

    @Test
    public void testGetMitiques() throws Exception {
        Observable<List<Mitica>> observable = repository.getMitiques(1);
        TestSubscriber<List<Mitica>> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();
        String observingThread = testSubscriber.getLastSeenThread().getName();
        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();
        List<List<Mitica>> onNextEvents = testSubscriber.getOnNextEvents();
        assertTrue(!onNextEvents.isEmpty());

        testSubscriber.assertCompleted();
    }

    @Test
    public void testGetTopMitiques() throws Exception {
        Observable<List<Mitica>> observable = repository.getTopMitiques(1);
        TestSubscriber<List<Mitica>> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();
        String observingThread = testSubscriber.getLastSeenThread().getName();
        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();
        List<List<Mitica>> onNextEvents = testSubscriber.getOnNextEvents();
        assertTrue(!onNextEvents.isEmpty());

        testSubscriber.assertCompleted();
    }

    @Test
    public void testGetMitiquesPerFacultat() throws Exception {
        Observable<List<Mitica>> observable = repository.getMitiquesPerFacultat(1,1);
        TestSubscriber<List<Mitica>> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();
        String observingThread = testSubscriber.getLastSeenThread().getName();
        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();
        List<List<Mitica>> onNextEvents = testSubscriber.getOnNextEvents();
        assertTrue(!onNextEvents.isEmpty());

        testSubscriber.assertCompleted();
    }

    @Test
    public void testGetMitiquesPerProfe() throws Exception {
        Observable<List<Mitica>> observable = repository.getMitiquesPerProfe(1,10);
        TestSubscriber<List<Mitica>> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();
        String observingThread = testSubscriber.getLastSeenThread().getName();
        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        testSubscriber.assertNoErrors();
        List<List<Mitica>> onNextEvents = testSubscriber.getOnNextEvents();
        assertTrue(!onNextEvents.isEmpty());

        testSubscriber.assertCompleted();
    }

    @Test
    public void getMitiquesMultipage() {

        //seteamos una lista de eventos (en este caso string ya que el tipo de evento no nos importa.
        List<String> eventList = Arrays.asList("1","2","3","4");

        //por cada evento emitimos un nuevo valor de un rango de 1 a n donde ese valor será el que usemos para seleccionar la página del servicio
        Observable<List<Mitica>> observable = Observable.from(eventList).zipWith(Observable.range(1, 5), (s, integer) -> integer)
                .flatMap(repository::getTopMitiques);

        TestSubscriber<List<Mitica>> testSubscriber = new TestSubscriber<>();
        observable.subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();
        String observingThread = testSubscriber.getLastSeenThread().getName();

        //comprovamos que estamos en el Scheduler de io
        assertThat(observingThread, containsString("RxCachedThreadScheduler"));

        //aseguramos que no llegue ningun evento de error
        testSubscriber.assertNoErrors();

        List<List<Mitica>> onNextEvents = testSubscriber.getOnNextEvents();

        //dado que solo hemos emitido 4 eventos solo reciviremos 4 respuestas.
        assertTrue(onNextEvents.size() == 4);

        testSubscriber.assertCompleted();
    }
}