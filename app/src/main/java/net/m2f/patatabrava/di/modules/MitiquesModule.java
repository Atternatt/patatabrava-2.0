/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.di.modules;

import android.content.Context;

import net.m2f.patatabrava.R;
import net.m2f.patatabrava.app.mitiqueslist.MitiquesListViewModel;
import net.m2f.patatabrava.app.mitiqueslist.MitiquesViewModelType;
import net.m2f.patatabrava.di.PerActivity;
import net.m2f.patatabrava.domain.interactor.mitiques.GetAllMitiques;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@Module
public class MitiquesModule {


    @Provides @IntoMap @PerActivity
    @IntKey(MitiquesViewModelType.ALL)
    public MitiquesListViewModel providesGellAllMitiquesViewModel(GetAllMitiques getAllMitiques, Context context) {
        MitiquesListViewModel mitiquesListViewModel = new MitiquesListViewModel(getAllMitiques);
        mitiquesListViewModel.setTitol(context.getString(R.string.all_mitiques));
        return mitiquesListViewModel;
    }
}
