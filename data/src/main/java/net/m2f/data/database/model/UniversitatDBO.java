/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.data.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
@DatabaseTable(tableName = UniversitatDBO.TABLE_NAME)
public class UniversitatDBO extends BaseEntity {

    public static final String TABLE_NAME = "unis_cache";

    public static final String FIELD_ID = "id";

    public static final String FIELD_NOM = "nom";
    public static final String FIELD_AMBIT_ID = "ambit_id";

    @DatabaseField(columnName = FIELD_ID, id = true)
    private final int id;

    @DatabaseField(columnName = FIELD_NOM)
    private final String nom;

    @DatabaseField(columnName = FIELD_AMBIT_ID)
    private int ambitId;

    public UniversitatDBO(int id, String nom, int ambitId) {
        this.id = id;
        this.nom = nom;
        this.ambitId = ambitId;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getAmbitId() {
        return ambitId;
    }

    @Override
    public String toString() {
        return "Universitat{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }

}
