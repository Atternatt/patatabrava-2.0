/*
 *
 *  * Copyright (c) 2016 Marc Moreno
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.m2f.patatabrava.app.mitiqueslist.rx;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.m2f.patatabrava.app.util.EndlessRecyclerOnScrollListener;

import rx.Observable;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

/**
 * @author Marc Moreno
 * @version 1.0
 */
final class RecyclerViewEndlessScrollOnSubscribe implements Observable.OnSubscribe<Integer>{

    final RecyclerView recyclerView;

    public RecyclerViewEndlessScrollOnSubscribe(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Override
    public void call(Subscriber<? super Integer> subscriber) {
        if (null == recyclerView.getLayoutManager()) {
            throw new RuntimeException("RecyclerView must have a LinearLayoutManager setted");
        } else if (!(recyclerView.getLayoutManager() instanceof LinearLayoutManager)) {
            throw new RuntimeException("LayoutManager must be of type LinearLayoutManager");
        }

        EndlessRecyclerOnScrollListener listener = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(current_page);
                }
            }
        };

        if (!subscriber.isUnsubscribed()) {
            subscriber.onNext(1);
        }

        recyclerView.addOnScrollListener(listener);

        subscriber.add(new MainThreadSubscription() {
            @Override
            protected void onUnsubscribe() {
                recyclerView.removeOnScrollListener(listener);
            }
        });
    }
}
